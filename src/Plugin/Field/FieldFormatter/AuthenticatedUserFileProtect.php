<?php

/**
 * @file
 * Contains \Drupal\authenticated_user_file_protect\Plugin\Field\FieldFormatter\AuthenticatedUserFileProtect
 */

namespace Drupal\authenticated_user_file_protect\Plugin\Field\FieldFormatter;

use Drupal\Core\Field\FormatterBase;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Field\FieldDefinitionInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
// use Drupal\Core\Entity\EntityInterface;
use Drupal\file\Entity\File;

/**
 * Plugin implementation of the 'ffawe_formatter' formatter.
 *
 * @FieldFormatter(
 *   id = "authenticated_user_file_protect",
 *   label = @Translation("Authenticated User File Protect"),
 *   field_types = {
 *     "file",
 *   }
 * )
 */
class AuthenticatedUserFileProtect extends FormatterBase {

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'form_behavior' => 'redirect',
      'form_type' => 'login',
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {

    $element['form_behavior'] = [
      '#title' => $this->t('Form Behavior'),
      '#type' => 'select',
      '#options' => [
        'redirect' => $this->t('Redirect to Form'),
        'show' => $this->t('Show Form Inline'),
      ],
      '#default_value' => $this->getSetting('form_behavior') ?: 'redirect',
    ];

    $element['form_type'] = [
      '#title' => $this->t('Form Type'),
      '#type' => 'select',
      '#options' => [
        'login' => $this->t('Login form'),
        'register' => $this->t('Register form'),
      ],
      '#default_value' => $this->getSetting('form_type') ?: 'login',
    ];

    return $element;

  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {

    $summary = [];
    $settings = $this->getSettings();
    $summary[] = $this->t('Form Behavior: ') . $settings['form_behavior'];
    $summary[] = $this->t('Form Type: ') . $settings['form_type'];

    return $summary;

  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $files, $langcode) {

    // Instatiate variables.
    $build = [];
    $message = [];
    $valueReturn = [];

    // Get form values.
    $formBehavior = $this->getSetting('form_behavior');
    $formType = $this->getSetting('form_type');
  

    foreach ($files as $delta => $file) {

      $getFileName = $file->entity->getFileName();

      // Check if user is anonymous.
      if (\Drupal::currentUser()->isAnonymous()) {

        // Redirect user to form.
        if ($formBehavior == 'redirect') {

          // Set custom filename value.
          $fileName = 'Protected file: ' . $getFileName;

          // Get current NID.
          $nodeIDPath = '/node/' . currentNodeID();

          // Set link custom options.
          $options = [
            'attributes' => [
              'class' => [
                'use-ajax',
                'protected-file-link',
              ],
              'data-dialog-type' => [
                'modal',
              ],
            ],
          ];

          // Create URL to register form.
          if ($formType == 'register') {

            // Set our custom file  message.
            $message = 'Please register to view this file.';

            // Set form URL to register form.
            $formURL = '/user/register?destination=' . $nodeIDPath;

          }

          // Create URL to login form.
          if ($formType == 'login') {

            // Set our custom file  message.
            $message = 'Please login to view this file.';

            // Set form URL to login form.
            $formURL = '/user/login?destination=' . $nodeIDPath;

          }

          // Build custom link.
          $build = \Drupal::l($fileName, Url::fromUserInput($formURL, $options));

        }

        // Show form inline on page.
        if ($formBehavior == 'show') {

          // Show register form.
          if ($formType == 'register') {

            // Set our custom file  message.
            $message = 'To view ' . $getFileName . ', please register.';

            // Get user register form.
            $entity = \Drupal::entityTypeManager()->getStorage('user')->create(array());
            $formObject = \Drupal::entityTypeManager()
              ->getFormObject('user', 'register')
              ->setEntity($entity);
            $form = \Drupal::formBuilder()->getForm($formObject);

            // Build user register form.
            $build = \Drupal::service('renderer')->render($form);

          }

          // Show login form.
          if ($formType == 'login') {

            // Set our custom file  message.
            $message = 'To view ' . $getFileName . ', please login.';

            // Get user login form.
            $form = \Drupal::formBuilder()->getForm(\Drupal\user\Form\UserLoginForm::class) ;

            // Build user login form.
            $build = \Drupal::service('renderer')->renderRoot($form);

          }

        }

      }
      // User is authenticated, return link to file.
      elseif (!\Drupal::currentUser()->isAnonymous()) {

        // Get filename.
        $fileName = $getFileName;

        // Build link.
        $build = \Drupal::l($fileName, Url::fromUri(File::load(1)->url()));

      }

      // Create new markup.
      $markup = new TranslatableMarkup(
        '<div class="file-protect-message">@message</div> @build', [
          '@message' => $message != NULL ? $message : '',
          '@build' => $build != NULL ? $build : '',
        ]
      );

      // Set our return array.
      $valueReturn[$delta] = [
        '#markup' => $markup,
      ];

    }

    return $valueReturn;

  }

  public static function isApplicable(FieldDefinitionInterface $field_definition) {
    return ($field_definition->getFieldStorageDefinition()->getSetting('target_type') == 'file');
  }

}
